from discord.ext import commands
from utils.scraper import Scraper

client = commands.Bot(command_prefix='./')
scraper = Scraper()


@client.event
async def on_ready():
    print('ready')


@client.command()
async def thxbot(ctx):
    await ctx.send("you're welcome!")


@client.command()
async def commands(ctx):
    await ctx.send("```./lunch         --- returns the lunch menu for regs and commons                 \n"
                   "./dinner        --- returns the dinner menu for regs and commons                \n"
                   "./breakfast     --- returns the menu for breakfast and shows which is open      \n"
                   "./cbreakfast    --- returns the menu for continetal breakfast and which is open \n"
                   "./brunch        --- returns the menu for brunch and shows which is open```")


@client.command()
async def lunch(ctx):
    scraper.set_date()
    await ctx.send(scraper.get_lunch())


@client.command()
async def dinner(ctx):
    scraper.set_date()
    await ctx.send(scraper.get_dinner())


@client.command()
async def breakfast(ctx):
    scraper.set_date()
    await ctx.send(scraper.get_breakfast())


@client.command()
async def cbreakfast(ctx):
    scraper.set_date()
    await ctx.send(scraper.get_c_breakfast())


@client.command()
async def brunch(ctx):
    scraper.set_date()
    await ctx.send(scraper.get_brunch())


client.run('Njc2NjM4OTU0MTYxODMxOTM3.XkInbQ.U6R_f-3W7WdS4nJyEe-BLoGF5ps')
