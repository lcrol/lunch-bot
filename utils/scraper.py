import apiclient.discovery as disc
import datetime
import pickle


# noinspection PyTypeChecker,PyTypeChecker
class Scraper:

    def __init__(self):

        self.regs_daily_events = []
        self.commons_daily_events = []
        self.cred = pickle.load(open('../client/token.pkl', 'rb'))
        self.service = disc.build("calendar", 'v3', credentials=self.cred)
        self.calendars = self.service.calendarList().list().execute()

        self.commons_id = self.calendars['items'][0]['id']
        self.regs_id = self.calendars['items'][4]['id']
        self.today = str(-1)
        self.set_date()
        self.time_min = self.today + "T00:00:00+00:00"
        self.commons_events = self.service.events().list(calendarId=self.commons_id, timeMin=self.time_min,
                                                         maxResults=2500, singleEvents=True).execute()
        self.regs_events = self.service.events().list(calendarId=self.regs_id, timeMin=self.time_min,
                                                      maxResults=2500, singleEvents=True).execute()
        self.set_regs_daily_events()
        self.set_commons_daily_events()

    def set_regs_daily_events(self):
        for i in self.regs_events['items']:
            try:
                this_date_time = i['start']['dateTime']
                if int(this_date_time[:4]) < 2020:
                    continue

                event_day = this_date_time[:len(self.today)]
                if event_day == self.today:
                    self.regs_daily_events.append(i)
            except all:
                continue

    def set_commons_daily_events(self):
        for i in self.commons_events['items']:
            try:
                event_date_time = i['start']['dateTime']
                if int(event_date_time[:4]) < 2020:
                    continue

                event_day = event_date_time[:len(self.today)]
                if event_day == self.today:
                    self.commons_daily_events.append(i)
            except all:
                continue

    def get_c_breakfast(self):
        return self.get('Continental Breakfast')

    def get_breakfast(self):
        return self.get("Breakfast")

    def get_brunch(self):
        return self.get('Brunch')

    def get_lunch(self):
        return self.get('Lunch')

    def get_dinner(self):
        return self.get('Dinner')

    def get(self, token):
        events = []
        for i in range(len(self.regs_daily_events)):
            if self.regs_daily_events[i]['summary'] == token:
                events.append(self.regs_daily_events[i])
                break

            if i == len(self.regs_daily_events) - 1:
                events.append('REGS IS CLOSED')

        for i in range(len(self.commons_daily_events)):
            if self.commons_daily_events[i]['summary'] == token:
                events.append(self.commons_daily_events[i])
                break
            if i == len(self.commons_daily_events) - 1:
                events.append('COMMONS IS CLOSED')

        form = ""

        for i in range(len(events)):
            if events[i] == 'REGS IS CLOSED' or events[i] == 'COMMONS IS CLOSED':
                form += events[i] + '\n\n'
                continue
            if i == 0:
                form += "```diff\n"
                form += "-REGS: \n\n" + events[i]['summary'] + '\n' + events[i]['description'] + "\n\n"
                form += "```"
            if i == 1:
                form += "```diff\n"
                form += "-COMMONS: \n\n" + events[i]['summary'] + '\n' + events[i]['description'] + "\n\n"
                form += "```"

        return form

    def set_date(self):
        self.today = str(datetime.datetime.now())[0:10]

